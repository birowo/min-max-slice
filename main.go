package main

import (
	"golang.org/x/exp/constraints"
)

func Min[T constraints.Ordered](x, y T) bool {
	return x < y
}
func Max[T constraints.Ordered](x, y T) bool {
	return x > y
}

type Cmp[T any] func(T, T) bool

func (cmp Cmp[T]) MinMax(slc ...T) (min T) {
	min = slc[0]
	for _, item := range slc[1:] {
		if cmp(item, min) {
			min = item
		}
	}
	return
}

var (
	MinInt = Cmp[int](Min[int]).MinMax
	MaxInt = Cmp[int](Max[int]).MinMax
)

func main() {
	println("min:", MinInt(
		3, 4, 1, 6, 2, 5,
	))
	println("max:", MaxInt(
		5, 2, 6, 1, 4, 3,
	))
}
